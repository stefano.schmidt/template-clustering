./gstlal_bank_splitter --output-path ./bank_split \
	--overlap 10 \
	--instrument H1 \
	--n 150 \
	--sort-by mchirp \
	--output-cache ./bank_split/split.cache \
	--f-low 15 \
	--approximant  0.00:1000.0:IMRPhenomHM \
	--max-f-final 2048 \
	../banks/bank_other_bbh.xml.gz
#	../banks/bank_other_bbh.xml.gz
#	./SBANK_COMBINED-HM_SBANK_PIPE.xml.gz
