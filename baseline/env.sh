source /home/gstlalcbc/testing/O2_replay/online/trigs.mario_replay_clone/build/gcc_deps_20200707/deps_env.sh
LAL_PATH=/home/stefano.schmidt/lscsoft/precession_search/opt
export CFLAGS="${CFLAGS} -I${LAL_PATH}/include"
export LD_LIBRARY_PATH=${LAL_PATH}/lib:${LD_LIBRARY_PATH}
export LIBRARY_PATH=${LAL_PATH}/lib:${LIBRARY_PATH}
# Force explicit linking of optimized FFTW libraries:
LDFLAGS="${LDFLAGS} -L/home/stefano.schmidt/lscsoft/precession_search/opt/lib "
# These are environment variables that do get exported
PATH=${LAL_PATH}/bin:${PATH}
PKG_CONFIG_PATH=${LAL_PATH}/lib/pkgconfig:${LAL_PATH}/lib64/pkgconfig:${PKG_CONFIG_PATH}
PYTHONPATH=${LAL_PATH}/lib/python2.7/site-packages:${LAL_PATH}/lib64/python2.7/site-packages:${PYTHONPATH}
LAL_DATA_PATH=/home/cbc/ROM_data
GST_PLUGIN_PATH=${LAL_PATH}/lib/gstreamer-1.0:${GST_PLUGIN_PATH}
GST_REGISTRY_1_0=${LAL_PATH}/registry.bin
GI_TYPELIB_PATH=${LAL_PATH}/lib/girepository-1.0:${GI_TYPELIB_PATH}
KAFKA_JVM_PERFORMANCE_OPTS="-server -XX:MetaspaceSize=96m -XX:+UseG1GC -XX:MaxGCPauseMillis=20 -XX:InitiatingHeapOccupancyPercent=35 -XX:G1HeapRegionSize=16M -XX:MinMetaspaceFreeRatio=50 -XX:MaxMetaspaceFreeRatio=80"
KAFKA_HEAP_OPTS="-Xms8G -Xmx8G"
export PATH PKG_CONFIG_PATH PYTHONPATH GST_PLUGIN_PATH KAFKA_PATH LAL_PATH LDFLAGS LDFLAGS_INTEL LAL_DATA_PATH GST_REGISTRY_1_0 GI_TYPELIB_PATH KAKFA_JVM_PERFORMANCE_OPTS KAFKA_HEAP_OPTS
export GSTLAL_FIR_WHITEN=0
