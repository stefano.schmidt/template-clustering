import numpy as np

import matplotlib
matplotlib.use('Agg')
from matplotlib import cm
import matplotlib.pyplot as plt

import sys

suffix = sys.argv[1]

data = np.loadtxt('plot_files/cluster_data_{}.dat'.format(suffix)) #(N_data, 5)
svd_components = np.loadtxt('plot_files/svd_comp_{}.dat'.format(suffix)) #(N_cl,2)
ids_sort = np.argsort(svd_components[:,0])
svd_components = svd_components[ids_sort,:]

colors = np.zeros((data.shape[0],))
for i in range(svd_components.shape[0]):
	ids = np.where(data[:,4] == i)
	colors[ids] = svd_components[i,1]

fig, ax = plt.subplots(1,1, figsize = (20,20))
#color_id = svd_components[data[:,2].astype(np.int32),1] #(N,)
#color_id = (color_id-min(color_id))/(max(color_id)-min(color_id))
s = ax.scatter(data[:,0], data[:,1], c = colors, cmap =  cm.get_cmap(), edgecolors = 'face')
cb = plt.colorbar(s)
ax.tick_params(axis='both', which='both', labelsize=20)
cb.ax.tick_params(labelsize=20)
plt.tight_layout()

#fig.savefig('bank_colors.png')
fig.savefig('/home/stefano.schmidt/public_html/template-clustering/{}/bank_SVD_{}.png'.format(suffix,suffix))


fig, ax = plt.subplots(1,1, figsize = (20,20))
#color_id = svd_components[data[:,2].astype(np.int32),1] #(N,)
#color_id = (color_id-min(color_id))/(max(color_id)-min(color_id))
s = ax.scatter(data[:,2], data[:,3], c = colors, cmap =  cm.get_cmap(), edgecolors = 'face')
cb = plt.colorbar(s)
ax.tick_params(axis='both', which='both', labelsize=20)
cb.ax.tick_params(labelsize=20)
plt.tight_layout()

#fig.savefig('bank_colors.png')
fig.savefig('/home/stefano.schmidt/public_html/template-clustering/{}/bank_SVD_prime_{}.png'.format(suffix,suffix))

	#histogram with SVD components
fig = plt.figure()
plt.hist(svd_components[:,1])
plt.tight_layout()

fig.savefig('/home/stefano.schmidt/public_html/template-clustering/{}/SVD_histogram_{}.png'.format(suffix,suffix))

