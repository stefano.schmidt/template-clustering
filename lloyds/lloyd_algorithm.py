"""
Small helper to implement the Lloyd algorithm on a metric space.
Distance between two points is given py
	d(x,y) = x^T A y
where A is a symmetric, positive definite matrix.
"""
import numpy as np
import scipy.spatial
import scipy.linalg
import matplotlib.pyplot as plt
import time
import os, shutil

#TODO: find good arrest condition

def lloyds_clustering(N_clusters, data_points, A_matrix, n_iter, box_factor = .02, plot= False):
	"""
	Performs a clustering of data_points based on the metric A_matrix.
	Inputs:
		N_cluster				Number of clusters
		data_points (N,D)		Points to cluster 
		A_matrix (D,D)			Distance matrix
		n_iter					number of iteration for the algorithm
		box_factor				the box over which all the computation happen is given by the data plus box_factor * border
		plot					whether to plot the data
	Ouputs:
		clusters (N,)				cluster assignment for each datapoint
		centroids (N_clusters, D)	centroids used for clustering
	"""
	if plot:
		import imageio
	plot_prime = True

	L = np.linalg.cholesky(A_matrix)
	L_scipy = scipy.linalg.cholesky(A_matrix).T
	L_inv = np.linalg.inv(L)
	
	print("A",A_matrix)
	print("A_choelsky", np.dot(L,L.T.conj()))
	print("A_scipy", np.dot(L_scipy,L_scipy.T.conj()))
	print("L", L)
	print("L_scipy", L_scipy)

		#intializing centroids
	centroids_ids = np.random.choice(data_points.shape[0], N_clusters)
	centroids = data_points[centroids_ids,:] #(N_cluster, D)

	centroids_prime = np.matmul(centroids,L.T)
	data_points_prime = np.matmul(data_points,L.T)
	
	box_lims = [data_points_prime.min(axis = 0), data_points_prime.max(axis = 0)]
	box_lims = [box_lims[0] -np.abs(box_lims[0])*box_factor, box_lims[1] +np.abs(box_lims[1])*box_factor ]
	
	if plot:
		try:
			shutil.rmtree('temp_png')
		except:
			pass
		plt.ion()
		fig = plt.figure()
		ax = plt.gca()

	for n in range(n_iter):
		print('Iteration {}'.format(n))
		
		mc_points_prime = np.random.uniform(*box_lims,size =  (1000000, data_points.shape[1]))
		#mc_points_prime = np.matmul(mc_points,L.T)
		voronoi_kdtree = scipy.spatial.cKDTree(centroids_prime) 

		#computing distances
		mc_points_dist, mc_points_regions = voronoi_kdtree.query(mc_points_prime) #(N,) #returns the region in which every point is located (i.e. the id of the closest centroid)

		#print(mc_points_dist, mc_points_regions)

		#computing size of regions and updating centroids
		unique, counts = np.unique(mc_points_regions, return_counts=True)
		print("Areas dict: ",dict(zip(unique, counts/float(mc_points_prime.shape[0]))))
		areas = counts/float(mc_points_prime.shape[0])
		#print(np.mean(areas), np.std(areas))

			#plotting cells
		if plot:
			if plot_prime:
				centroids_ = centroids_prime
				data_points_ = data_points_prime
				mc_points = mc_points_prime
			else:
				data_points_ = data_points
				centroids_ = np.matmul(centroids_prime, L_inv.T)
				mc_points = np.matmul(mc_points_prime, L_inv.T)
		
			vor = scipy.spatial.Voronoi(centroids_)
			
			ax.clear()
			ax.set_title('Iteration {}'.format(n))

			if plot_prime: scipy.spatial.voronoi_plot_2d(vor, ax )

			ax.plot(*mc_points[:1000,:].T, marker ='o', ms = .1, c= 'r')

			_, assignments = voronoi_kdtree.query(data_points_)
			for i in range(centroids.shape[0]):
				ids_ = np.where(assignments == i)[0]
				if len(ids_) == 0: pass
				ax.plot(*data_points_[ids_,:].T, marker= 'o', ms = 3)

			fig.canvas.draw_idle()
			try:
				plt.savefig("temp_png/{}.png".format(n))
			except FileNotFoundError:
				os.mkdir('temp_png')
				plt.savefig("temp_png/{}.png".format(n))
			plt.pause(1.)

			#updates centroids
		for i in range(centroids_prime.shape[0]):
			ids_ = np.where(mc_points_regions == i)[0]
			dist = scipy.spatial.distance.cdist(mc_points_prime[ids_,:], centroids_prime[None,i,:]) #(D,)
			centroid = np.average(mc_points_prime[ids_,:], axis = 0, weights=  np.squeeze(dist))

			centroids_prime[i,:] = centroid

	centroids = np.matmul(centroids_prime, L_inv.T)
	
	data_points_dist, data_points_regions = voronoi_kdtree.query(data_points_prime) #(N,) #returns the region in which every point is located (i.e. the id of the closest centroid)

		# Remove files
	if plot:
		with imageio.get_writer('Lloyd.gif', mode='I', duration = .5) as writer:
			for i in range(len(os.listdir('temp_png'))):
				image = imageio.imread('temp_png/{}.png'.format(i))
				writer.append_data(image)
			for j in range(7):
				writer.append_data(image)
		shutil.rmtree('temp_png')

	if plot:
		plt.close()
		plt.ioff()

	return data_points_regions, centroids


if __name__ == "__main__":
	A = np.array([[3,0],[0.,3.]])

	theta = np.radians(45)

	r = np.array(( (np.cos(theta), -np.sin(theta)),
		           (np.sin(theta),  np.cos(theta)) ))
	A = np.matmul(r.T,np.matmul(A,r))


	data_ = np.random.normal(0,1,(2000,2)) +  np.random.normal(2,.7,(2000,2)) 

	assignments, centroids = lloyds_clustering(5, data_, A, 20, .2, True)

	print(centroids.shape)

	fig = plt.figure(0)
	ax = plt.gca()
	vor = scipy.spatial.Voronoi(centroids)
	scipy.spatial.voronoi_plot_2d(vor, ax)

	for i in range(centroids.shape[0]):
		#ax.plot(centroids[i,0], centroids[i,1], 'o', ms = 3, c ='k')
		ids_ = np.where(assignments == i)[0]
		if len(ids_) == 0: pass
		print(data_[ids_,:].shape)
		ax.plot(*data_[ids_,:].T, marker = 'o', ms = 2)

	plt.show()
		












