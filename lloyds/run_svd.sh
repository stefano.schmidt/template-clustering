../baseline/measure_svd --reference-psd ../baseline/H1L1V1-REFERENCE_PSD-1241800850-86400.xml.gz \
	--samples-min 1024 \
	--bank-id 10\
	--ortho-gate-fap 0.5 \
	--flow 40.0 \
	--template-bank bank_split/ \
	--svd-tolerance 0.9995 \
	--write-svd-bank svd_split/svd_comp_$1.dat \
	--samples-max-64 4096 \
	--clipleft 0 \
	--autocorrelation-length 351 \
	--samples-max-256 3024 \
	--clipright 20 \
	--samples-max 40 
#	--verbose

