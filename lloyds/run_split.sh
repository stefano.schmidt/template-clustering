./gstlal_bank_splitter --suffix std \
	--output-path ./bank_split \
	--overlap 10 \
	--instrument H1 \
	--n 100 \
	--group-by-chi 20 \
	--sort-by mchirp \
	--output-cache ./bank_split/split.cache \
	--f-low 15 \
	--approximant  0.00:1000.0:IMRPhenomHM \
	--max-f-final 2048 \
	../banks/bank_imbh.xml.gz
