# template-clustering

A clustering method for the templates in a GW bank, used in searches.
The goal is to integrate the clustering algorithm into the GstLAL pipeline to achieve better performances on the SVD decomposition.

## Our work

In the GstLAL pipeline, SVD decomposition is very important for speeding up the matched filetering procedur by drastically reducing the dimensionality of the WFs. The SVD is not applied to the whole bank but instead on smaller subset (clusters) of the bank.
Currently the clustering is based upon chirp mass and effective spin parameters, splitting the templates on a square grid (see [arxiv.org/abs/1604.04324](https://arxiv.org/abs/1604.04324) for more details).
There are reason to believe that such splitting method is suboptimal, especially for very large banks (as those required for precessing signals detections).
Our goal here is to find a better clustering algorithm, which possibly allows for better SVD performance.

We follow two main threads of work:

### Lloyd's algorithm
A distance is built on the parameter space of the templates and with some sort of optimization, we try to get the right metric on the parameter space.
Once a metric is available, the Lloyd's algorithm will "evenly" spread some points across the space. Each point is a centroid for the k-mean clustering algorithm.

See: https://en.wikipedia.org/wiki/Lloyd%27s_algorithm and
https://en.wikipedia.org/wiki/Farthest-first_traversal

### Iterative clustering building
The idea is to write down a distance beetween clusters D(X,Y). In our case it would be naturally defined as the # of SVD components required to achieve a given reconstruction error in the set Z = X U Y
Once we have a distance, we could recursively merge two nearest neighbour clusters until we achieve a satisfying clustering. This should be an optimal way of doing things: it is a good heuristics for the very hard problem.
The algorithm usually starts with singleton clusters; for computational reasons, we probably want to start with larger (but still small) clusters.

See: https://en.wikipedia.org/wiki/Nearest-neighbor_chain_algorithm and https://en.wikipedia.org/wiki/Ward%27s_method

## Usage of the code
Ultimately, the only change to gstlal should be made in the GstLAL command `gstlal_bank_splitter` (and possibily in some other script to import).
A local version of the GstLAL command to be run is stored in folder [baseline](https://github.com/stefanoschmidt1995/template-clustering/tree/main/baseline) (see [here](https://lscsoft.docs.ligo.org/gstlal/gstlal-inspiral/bin/gstlal_bank_splitter.html) for documentation). 
In the file, we need to edit from line 170-186 of the [file](https://git.ligo.org/lscsoft/gstlal/-/blob/O3_precessionHM/gstlal-inspiral/bin/gstlal_bank_splitter) and replace them with a new clustering method.
The code there outputs a bunch of `SPLIT_BANK` files that are ready to be given in input to `gstlal_svd_bank` (a copy is also provided in baseline), which performs the SVD. 

The code could be run as a stand alone, provided that a local installation of GstLAL is available.

If you run on LIGO cluster (e.g. `@ldas-pcdev11.ligo.caltech.edu`) you might want to source the file `env.sh`.
You can also use bash script `run_cmd.sh` to call the bank splitter with some predefined options (or take it as an example).
A bank file `SBANK_COMBINED-HM_SBANK_PIPE.xml.gz` is also provided for testing purposes.

A (bash) script `measure_svd` is useful to measure the number of SVD components required for reliable decomposition: it uses GstLAL command `gstlal_svd_bank` for the SVD. It takes in input a folder with a bunch of SVD split files (i.e. the output of `gstlal_bank_splitter`) and saves to file the number of SVD components required to represent each bank split (i.e. cluster). It is useful for assessing the algorithm performance (and probably also for training).

All the code described above is stored in `baseline`, which gathers the GstLAL existing code that is relevant to the project and some scripts common to all the attempts.

All the work (and changes) shall be made in the following folder, by coping the useful code in baseline and adding new one:

* **lloyds**: gathers everything required for the Lloyd's algorithm approach
* **hierarchical**: collects the code useful for the hierarchical clustering approach

## To-dos

Here you can find a list of to-dos:

* Finish the project and get a Nobel prize
* Metric performance: decide what they are and implement them
* Other...

## Authors

+ Quirijn Meijer: [r.h.a.j.meijer@uu.nl](mailto:r.h.a.j.meijer@uu.nl)
+ Stefano Schmidt: [s.schmidt@uu.nl](mailto:s.schmidt@uu.nl)



















